<?php

return [
    'enabled_header'  => [
        'name'    => 'Enabled Header',
        'options' => [
            'every_page'    => 'Every Page',
            'homepage_only' => 'Homepage Only',
            'disabled'      => 'Disabled',
        ],
    ],
    'top_menu' => [
        'name'         => 'Top Menu',
        'instructions' => 'If no menu is selected, the page <strong>structure()</strong> will be used.',
    ],
    'inline_assets'   => [
        'name'         => 'Inline Assets',
        'instructions' => 'It is not recommanded to inline assets in HTTP/2.',
        'options'      => [
            'yes' => 'Yes',
            'no'  => 'No',
        ],
    ],
    'top_login' => [
        'name'    => 'Top Login',
        'instructions' => 'Show the Login/Logout in the top menu?',
        'options' => [
            'yes' => 'Yes',
            'no'  => 'No',
        ],
    ],
];
