<?php

return [
    [
        'tabs' => [
            'general'      => [
                'title'  => 'newebtime.theme.grayscale::tab.general',
                'fields' => [
                    'enabled_header',
                    'inline_assets',
                ],
            ],
            'navigation' => [
                'title'  => 'newebtime.theme.grayscale::tab.navigation',
                'fields' => [
                    'top_menu',
                    'top_login',
                ],
            ],
        ],
    ],
];
