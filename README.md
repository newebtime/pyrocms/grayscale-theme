# Grayscale Theme

This theme is a basic HTML5 Bootstrap theme based on the Grayscale Theme from https://startbootstrap.com/template-overviews/grayscale/

## Installation

```bash
composer require newebtime/grayscale-theme
```

For more details: https://pyrocms.com/documentation/pyrocms/3.4/installation/installing-addons

## Customization

The theme come with a pre-compiled CSS file, but it's also providing the SCSS to customise the theme.

### Install dependencies

#### Manually

Go to the theme folder and run `npm install`

#### Using minstall

Minstall allow to automatically install `package.json` dependencies from all addons.

```bash
npm install minstall --save
```

Then update PyroCMS `package.json`

```json
    "scripts": {
        ...
        "postinstall": "minstall addons/*/*"
    },
```

Then

```bash
npm install
```

### Using Assetic

1. Go in `resources/views/partials/metadata.twig`
2. Comment `{{ asset_add('theme.css', 'theme::css/theme.css', ['live', 'parse']) }}`
3. Uncomment `{#{{ asset_add('theme.css', 'theme::scss/theme.scss', ['live', 'parse']) }}#}`

### Using Mix / Webpack

1. Go in `resources/scss/theme.scss`
2. Comment all the `// Vendor - Assetic` section
3. Uncomment all the `// Vendor - Mix` section

Then configure `webpack.mix.js` to compile the scss

```js
require(`./addons/app/newebtime/grayscale-theme/webpack.mix.js`);
```

Caution: `addons/app` may differ depending of you application reference.

## Tested Module

The following modules were tested (well all module using bootstrap 4 should be compatible).

* Forms Module (anomaly.module.forms), `1.3`
* Pages Module (anomaly.module.pages), `2.4`
* Portfolio Module (newebtime.module.portfolio), `1.0.0`
* Posts Module (anomaly.module.posts), `2.4` (`2.5` is not tested yet)
  * Know bug: The `Tags` are not rendered correctly. Too many twig need to be reworked for it...
* Users Module (anomaly.module.users), `2.4`
