<?php

namespace Newebtime\GrayscaleTheme;

use Anomaly\Streams\Platform\Addon\Theme\Theme;

/**
 * Class GrayscaleTheme
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class GrayscaleTheme extends Theme
{

}
