<?php

namespace Newebtime\GrayscaleTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

/**
 * Class GrayscaleThemeServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class GrayscaleThemeServiceProvider extends AddonServiceProvider
{
    /**
     * {@inheritdoc}
     */
    protected $overrides = [
        'streams::errors/404'           => 'theme::errors/404',
        'streams::errors/500'           => 'theme::errors/500',
        'streams::form/partials/layout' => 'theme::form/partials/layout',

        'anomaly.module.posts::posts/partials/posts' => 'theme::posts/partials/posts', // Note: Doesn't work
    ];
}
